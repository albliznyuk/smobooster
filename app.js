require('dotenv').config();

const path = require('path'),
    osprey = require('osprey'),
    securityHandler = require('./api-key-access-policy');

const {initRouter, initDb, initApp} = require('./bootstrappers');

osprey.loadFile(path.join(__dirname, process.env.API_SPEC), {security: securityHandler})
    .then(function (raml) {
        initDb();

        const app = initApp();
        app.use('/' + process.env.API_VERSION, initRouter(raml));
        app.listen(process.env.PORT);
    })
    .catch(function(e) { console.error("Error: %s", e.message); });
