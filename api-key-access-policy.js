const express = require('express');

// osprey security param
module.exports = {
    apiKey(scheme, name) {
        return {
            handler(options, path) {
                return function (req, res, next) {
                    if (!req.headers['api-key']) {
                        return res.sendStatus(403);
                    }

                    express.clientsBucket.get(req.headers['api-key'], function (err, result) {
                        if (err || !result || !result.value || !result.value.active) {
                            return res.sendStatus(403);
                        }

                        req.state = {...result.value};
                        next();
                    });
                }

            }
        };
    }
};
