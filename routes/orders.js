const express = require('express'),
    router = express.Router(),
    uuidv4 = require('uuid/v4'),
    fetch = require('node-fetch'),
    { URLSearchParams } = require('url'),
    sendmail = require('sendmail')();


router.get('/:orderId', async function(req, res, next) {
    if (!req.state.orders || !req.state.orders.length) {
        return res.sendStatus(404);
    }

    const orderI = req.state.orders.findIndex(order => order.id === req.params.orderId);
    if (orderI === -1) {
        return res.sendStatus(404);
    }

    try {
        const order = req.state.orders[orderI];
        let panelResponse = getRemoteOrderState(service.value.panel, {orderId: order.panelId});
        panelResponse.rate = order.rate;

         express.clientsBucket
             .mutateIn(req.headers['api-key'])
             .replace(`orders[${orderI}]`, {...order, ...panelResponse})
             .execute();

         return res.json(panelResponse);

    } catch (e) {
        return res.status(500).send(e.message);
    }
});


router.post('/', function(req, res, next) {
    let newOrderId;
    express.servicesBucket.get(req.body.serviceId, async function(err, service) {
        if (err) {
            return res.status(400).send(err);
        }
        if (!service || !service.value) {
            return res.status(500).send();
        }

        let panelResponse = {};
        try {
            panelResponse = await createRemoteOrder(service.value.panel, {service, req});
            newOrderId = addOrder({panelId: panelResponse.order, service, req, res});
        } catch (e) {
            notifyAboutError({
                userId: req.headers['api-key'],
                orderId: newOrderId,
                panelOrderId: panelResponse.order,
                panelServiceId: service.value.panel_service,
                errorMessage: e.message
            });

            return res.status(500).send();
        }
    });
});

function notifyAboutError({userId, orderId, panelOrderId, panelServiceId, errorMessage}) {
    sendmail({
        from: 'notifications@freegram.ru',
        to: process.env.NOTIFICATIONS_EMAIL,
        subject: 'Failed to transfer order',
        text: `User id: ${userId}\n` +
            `Order id: ${orderId}\n` +
            `Panel:\n  order id: ${panelOrderId}\n  service id: ${panelServiceId}\n` +
            `Error: ${errorMessage}\n`,
    });
}

async function createRemoteOrder(url, {service, req}) {
    const params = new URLSearchParams();

    params.append('key', process.env.PANEL_KEY);
    params.append('action', 'add');
    params.append('service', service.value.panel_service);
    params.append('link', req.body.link);
    params.append('quantity', req.body.quantity);

    const panelResponse = await (await fetch(url, {
        method: 'POST',
        body: params
    })).json();

    if (panelResponse.error) {
        throw new Error(panelResponse.error);
    }

    return panelResponse;
}

function addOrder({service, req, panelId, res}) {
    const newOrderId = uuidv4();
    express.clientsBucket
        .mutateIn(req.headers['api-key'])
        .arrayAppend('orders', {
            rate: service.value.rate,
            service: req.body.serviceId,
            currency: 'USD',
            start_count: req.body.quantity,
            remains: req.body.quantity,
            charge: service.value.rate,
            date: Date.now(),
            status: 'Processing',
            link: req.body.link,
            id: newOrderId,
            panelId

        })
        .execute( function(err, result) {
            if (err) {
                return res.status(500).send(err);
            }

            res.json({orderId: newOrderId});

        });
    return newOrderId;
}

async function getRemoteOrderState(url, {orderId}) {
    const params = new URLSearchParams();
    params.append('key', process.env.PANEL_KEY);
    params.append('action', 'status');
    params.append('order', orderId);

    const panelResponse = await(await fetch(url, {
        method: 'POST',
        body: params
    })).json();
    if (panelResponse.error) {
        throw new Error(panelResponse.error);
    }
    return panelResponse;
}

module.exports = router;
