const logger = require('morgan'),
    rateLimit = require("express-rate-limit"),
    couchbase = require('couchbase'),
    express = require('express'),
    osprey = require('osprey');

function initRouter(raml) {
    const apiRouter = new osprey.Router();
    apiRouter.use(raml);
    apiRouter.use(function (err, req, res, next) {
        console.log(err)
        // Handle errors.
    });
    apiRouter.use('/orders', require('./routes/orders'));
    return apiRouter;
}

function initDb() {
    const cluster = new couchbase.Cluster(process.env.CB_CLUSTER);
    cluster.authenticate(process.env.CB_USER, process.env.CB_PASSWORD);

    express.clientsBucket = cluster.openBucket('clients');
    express.servicesBucket =  cluster.openBucket('services');
}

function initApp() {
    const app = express();
    app.use(logger('dev'));
    app.use(express.json());
    app.use(express.urlencoded({ extended: false }));
    app.use(rateLimit({
        windowMs: 60 * 1000,
        max: process.env.REQUESTS_PER_MINUTE_LIMIT
    }));
    return app;
}

module.exports = {initRouter, initDb, initApp};
